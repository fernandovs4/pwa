
importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.1.0/workbox-sw.js');



workbox.skipWaiting();

workbox.clientsClaim();



workbox.precaching.precacheAndRoute([]);



workbox.routing.registerRoute(/images\/products\/(.*)$/,

  workbox.strategies.cacheFirst({

    cacheName: 'furniture-store-images',

    plugins: [

      new workbox.expiration.Plugin({

        maxEntries: 10,

        maxAgeSeconds: 7 * 24 * 60 * 60 // one week

      })

    ]

  })

);



workbox.routing.registerRoute(

  new RegExp('https://fonts.(?:googleapis|gstatic).com/(.*)'),

  workbox.strategies.staleWhileRevalidate({

    cacheName: 'furniture-store-fonts',

    plugins: [

      new workbox.expiration.Plugin({

        maxAgeSeconds: 7 * 24 * 60 * 60 // one week

      })

    ]

  })

);



workbox.routing.registerRoute(

  new RegExp('https://code.getmdl.io/(.*)'),

  workbox.strategies.staleWhileRevalidate({

    cacheName: 'furniture-store-mdl',

    plugins: [

      new workbox.expiration.Plugin({

        maxAgeSeconds: 7 * 24 * 60 * 60 // one week

      })

    ]

  })

);